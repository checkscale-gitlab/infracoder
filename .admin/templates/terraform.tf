## --- terraform settings ---
terraform {
  required_version = "= 0.12.24"

  backend "s3" {
    endpoint   = "https://tboettjer.compat.objectstorage.us-phoenix-1.oraclecloud.com"
    region     = "us-phoenix-1"
    bucket     = "tfstate"
    key        = "terraform.tfstate"
    shared_credentials_file = "~/.aws/credentials"

    skip_region_validation      = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    force_path_style            = true
  }
}