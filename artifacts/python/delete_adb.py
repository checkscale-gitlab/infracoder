### Python Examples ##################################
##                                                  ##
##  create ADB, download wallet, connect to ADB     ##
##                                                  ##
##  Alexander Boettcher                             ##
######################################################


import oci
import sys
import argparse

# Load the default configuration
config = oci.config.from_file()

parser = argparse.ArgumentParser(description='Delete an autonomous database.')
parser.add_argument('-o','--ocid',help='OCID of autonomous database', required=True)
parser.add_argument('-co','--compocid',help='Compartment ocid', required=False)
args = parser.parse_args()

if args.compocid is not None:
  config["compartment"] = args.compocid
  compartment_id = args.compocid  
  print("Using compartment ocid: ",compartment_id)
else:
  compartment_id = config["compartment"]
  print("Using compartment ocid: ",compartment_id)

# get OCID from parameter
adb_id = args.ocid


def get_lifecycle_state(db_client):
    try:
      adb = db_client.get_autonomous_database(adb_id)
      return print("ADB in state: ",adb.data.lifecycle_state)
    except:
      print("ADB not existent?")

def delete_adb(db_client, adb_id):
    response = db_client.delete_autonomous_database(adb_id)

if __name__ == "__main__":
    # Initialize the client
    db_client = oci.database.DatabaseClient(config)
    try:
      delete_adb(db_client, adb_id)
      print("delete initiated.")
    except:
      print("Delete cannot be executed.")
      get_lifecycle_state(db_client)


