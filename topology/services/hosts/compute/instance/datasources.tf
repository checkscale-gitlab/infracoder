// Copyright (c) 2017, 2019, Oracle and/or its affiliates. All rights reserved.

########################
# availability domains #
########################

data "oci_identity_availability_domains" "ads" {
  compartment_id = "${var.tenancy_ocid}"
}