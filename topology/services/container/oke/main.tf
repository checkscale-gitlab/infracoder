
# --- creating the OKE cluster -----
resource "oci_containerengine_cluster" "oke_cluster" {
  #Required
  compartment_id     = var.compartment_ocid
  kubernetes_version = var.cluster_kubernetes_version
  name               = var.cluster_name
  vcn_id             = var.network_id

  options {
    #Optional
    add_ons {
      #Optional
      is_kubernetes_dashboard_enabled = var.cluster_options_add_ons_is_kubernetes_dashboard_enabled
      is_tiller_enabled               = var.cluster_options_add_ons_is_tiller_enabled
    }
    #kubernetes_network_config {
      #Optional
      #pods_cidr     = var.cluster_options_kubernetes_network_config_pods_cidr
      #services_cidr = var.cluster_options_kubernetes_network_config_services_cidr
    #}
    #service_lb_subnet_ids = var.cluster_options_service_lb_subnet_ids
  }
}

# --- adding a node pool to the cluster ----
resource "oci_containerengine_node_pool" "oke_node_pool" {
    #Required
    cluster_id = oci_containerengine_cluster.oke_cluster.id
    compartment_id = var.compartment_ocid
       
    kubernetes_version = var.cluster_kubernetes_version
    
    name = var.node_pool_name
    node_shape = var.node_pool_node_shape
    #subnet_ids = [oci_core_subnet.nodePool_Subnet_1.id, oci_core_subnet.nodePool_Subnet_2.id]
    subnet_ids = [var.nodePool_Subnet_1, var.nodePool_Subnet_2, var.nodePool_Subnet_3]

    #Optional
    #initial_node_labels {
    #
    #    #Optional
    #    key = "${var.node_pool_initial_node_labels_key}"
    #    value = "${var.node_pool_initial_node_labels_value}"
    #}
    
    #node_metadata = "var.node_pool_node_metadata"
        
    node_source_details {
        #Required
        image_id = var.node_image_id
        source_type = "IMAGE"
    }
    
    # Total number of nodes will this number times number of subnets
    quantity_per_subnet = 1
    
    ssh_public_key = var.node_pool_ssh_public_key
}

resource "local_file" "kubeconfigoci" {
  content  = data.oci_containerengine_cluster_kube_config.kube_config.content
  filename = "${path.module}/kubeconfig_oci"
}

