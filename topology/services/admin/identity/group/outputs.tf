// Copyright (c) 2017, 2019, Oracle and/or its affiliates. All rights reserved.

################
# group output #
################

output "groups" {
  value = "${data.oci_identity_groups.groups1.groups}"
}

output "group_name" {
  value = "${oci_identity_group.default-group.name}"
}