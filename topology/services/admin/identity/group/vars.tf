// Copyright (c) 2017, 2019, Oracle and/or its affiliates. All rights reserved.

###################
# group variables #
###################

variable "tenancy_ocid" {}
variable "user_id" {}
