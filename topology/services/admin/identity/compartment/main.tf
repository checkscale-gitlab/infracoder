// Copyright (c) 2017, 2019, Oracle and/or its affiliates. All rights reserved.

################################################
# creating a compartment and defining policies #
################################################

resource "oci_identity_compartment" "default-compartment" {
  name           = "DefaultCompartment"
  description    = "compartment created by terraform"
  compartment_id = "${var.tenancy_ocid}"
}

data "oci_identity_compartments" "compartments1" {
  compartment_id = "${oci_identity_compartment.default-compartment.compartment_id}"

  filter {
    name   = "name"
    values = ["DefaultCompartment"]
  }
}

resource "oci_identity_policy" "policy" {
  name           = "default-policy"
  description    = "policy created for default compartment"
  compartment_id = "${oci_identity_compartment.default-compartment.id}"

  statements = ["Allow group ${var.group_name} to manage all-resources in compartment ${oci_identity_compartment.default-compartment.name}"]
}