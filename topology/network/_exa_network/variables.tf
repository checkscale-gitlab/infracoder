#############################################################
###### variables.tf #########################################
######                                                  #####
###### OCI Exadata Cloud Service Network Implementation #####
###### Based on Oracle documentation as of 2019-09-18   #####
######                                                  #####
###### contact: alexander.boettcher@oracle.com          #####
#############################################################

######## Connection specific variables (filled via shell)  ##############

variable "tenancy_ocid" {
}

variable "user_ocid" {
}

variable "fingerprint" {
}

variable "private_key_path" {
}

variable "compartment_ocid" {
}

variable "ssh_public_key" {
}

variable "ssh_private_key" {
}

### All VCN Details
variable "VCN_NAME" {
  default = "DialogVCN"
}

variable "SN_ExaClient" {
  default = "SN_ExaClient"
}

variable "SN_ExaBackup" {
  default = "SN_ExaBackup"
}

variable "SN_mgmt" {
  default = "SN_MGMT"
}

variable "SN_Windows" {
  default = "SN_Windows"
}

variable "SN_Linux" {
  default = "SN_Linux"
}

variable "VCN_CIDR" {
  default = "10.5.0.0/16"
}

variable "SN_ExaClient_CIDR" {
  default = "10.5.70.0/24"
}

variable "SN_ExaBackup_CIDR" {
  default = "10.5.71.0/24"
}

variable "SN_mgmt_CIDR" {
  default = "10.5.100.0/24"
}

variable "SN_Windows_CIDR" {
  default = "10.5.16.0/23"
}

variable "SN_Linux_CIDR" {
  default = "10.5.28.0/22"
}


variable "SAP_ON_PREM_CIDR" {
  default = "172.21.61.0/24"
}

variable "ALL_ON_PREM_CIDR" {
  default = "10.0.0.0/8"
}

variable "ON_PREM_CIDR1" {
  default = "10.10.0.0/16"
}

variable "ON_PREM_CIDR2" {
  default = "10.20.0.0/16"
}

variable "ON_PREM_CIDR3" {
  default = "10.30.0.0/16"
}

variable "ON_PREM_CIDR4" {
  default = "10.40.0.0/16"
}

######## Instance specific vars (like shapes, OS, version) ################

## Image is Oracle Linux 7.7 as of OCI Provided Images for Frankfurt:
## https://docs.cloud.oracle.com/en-us/iaas/images/image/0a72692a-bdbb-46fc-b17b-6e0a3fedeb23/
variable "default_image" {
  default = "ocid1.image.oc1.eu-frankfurt-1.aaaaaaaa4xluwijh66fts4g42iw7gnixntcmns73ei3hwt2j7lihmswkrada"
}

variable "InstanceShape1" {
  default = "VM.Standard2.1"
}

variable "InstanceShape2" {
  default = "VM.Standard2.2"
}

variable "InstanceShape4" {
  default = "VM.Standard2.4"
}

variable "InstanceOS" {
  default = "Oracle Linux"
}

variable "InstanceOSVersion" {
  default = "7.6"
}

#########   CIDR  #################

variable "All-Internet-CIDR" {
  default = "0.0.0.0/0"
}

variable "VCN-CIDR" {
  default = "10.0.0.0/16"
}



######### Bootstrapping Files ##############

variable "InstanceBootStrap" {
  default = "./userdata/instance"
}


####### Ports #####

#variable "port_db_adw" {
#  default = "1522"
#}



