# API Gateway

```
## --- Call API Gateway Module ---
module "network" {
  source              = "./network/api_gateway"
  tenancy_ocid        = var.tenancy_ocid
  compartment_ocid    = var.compartment_ocid
  user_ocid           = var.user_ocid
  fingerprint         = var.fingerprint
  private_key_path    = var.private_key_path
  region              = var.region
}
```