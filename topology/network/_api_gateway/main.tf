resource "oci_functions_application" "test_application" {
  #Required
  compartment_id = var.compartment_ocid
  display_name   = "example-application"
  subnet_ids     = [oci_core_subnet.regional_subnet.id]

  #Optional
  config = var.config
}

# Terraform will take 40 minutes after destroying a function due to a known service issue.
# please refer: https://docs.cloud.oracle.com/iaas/Content/Functions/Tasks/functionsdeleting.htm
resource "oci_functions_function" "test_function" {
  #Required
  application_id = oci_functions_application.test_application.id
  display_name   = "example-function"
  image          = var.function_image
  memory_in_mbs  = "128"

  #Optional
  config             = var.config
  image_digest       = var.function_image_digest
  timeout_in_seconds = "30"
}

resource "oci_apigateway_gateway" "test_gateway" {
  #Required
  compartment_id = var.compartment_ocid
  endpoint_type  = var.gateway_endpoint_type
  subnet_id      = oci_core_subnet.regional_subnet.id

  #Optional
  display_name = var.gateway_display_name
}

resource "oci_apigateway_deployment" "test_deployment" {
  #Required
  compartment_id = var.compartment_ocid
  gateway_id     = oci_apigateway_gateway.test_gateway.id
  path_prefix    = var.deployment_path_prefix

  specification {
    #Optional
    logging_policies {
      #Optional
      access_log {
        #Optional
        is_enabled = var.deployment_specification_logging_policies_access_log_is_enabled
      }

      execution_log {
        #Optional
        is_enabled = var.deployment_specification_logging_policies_execution_log_is_enabled
        log_level  = var.deployment_specification_logging_policies_execution_log_log_level
      }
    }

    request_policies {
      #Optional
      authentication {
        #Required
        function_id = oci_functions_function.test_function.id
        type        = var.deployment_specification_request_policies_authentication_type

        #Optional
        is_anonymous_access_allowed = var.deployment_specification_request_policies_authentication_is_anonymous_access_allowed
        token_header                = var.deployment_specification_request_policies_authentication_token_header
      }

      cors {
        #Required
        allowed_origins = var.deployment_specification_request_policies_cors_allowed_origins

        #Optional
        allowed_headers              = var.deployment_specification_request_policies_cors_allowed_headers
        allowed_methods              = var.deployment_specification_request_policies_cors_allowed_methods
        exposed_headers              = var.deployment_specification_request_policies_cors_exposed_headers
        is_allow_credentials_enabled = var.deployment_specification_request_policies_cors_is_allow_credentials_enabled
        max_age_in_seconds           = var.deployment_specification_request_policies_cors_max_age_in_seconds
      }

      rate_limiting {
        #Required
        rate_in_requests_per_second = var.deployment_specification_request_policies_rate_limiting_rate_in_requests_per_second
        rate_key                    = var.deployment_specification_request_policies_rate_limiting_rate_key
      }
    }

    routes {
      #Required
      backend {
        #Required
        type = var.deployment_specification_routes_backend_type
        url  = var.deployment_specification_routes_backend_url
      }

      path = var.deployment_specification_routes_path

      #Optional
      logging_policies {
        #Optional
        access_log {
          #Optional
          is_enabled = var.deployment_specification_routes_logging_policies_access_log_is_enabled
        }

        execution_log {
          #Optional
          is_enabled = var.deployment_specification_routes_logging_policies_execution_log_is_enabled
          log_level  = var.deployment_specification_routes_logging_policies_execution_log_log_level
        }
      }

      methods = var.deployment_specification_routes_methods

      request_policies {
        #Optional
        authorization {
          #Optional
          type = var.deployment_specification_routes_request_policies_authorization_type
        }

        cors {
          #Required
          allowed_origins = var.deployment_specification_routes_request_policies_cors_allowed_origins

          #Optional
          allowed_headers              = var.deployment_specification_routes_request_policies_cors_allowed_headers
          allowed_methods              = var.deployment_specification_routes_request_policies_cors_allowed_methods
          exposed_headers              = var.deployment_specification_routes_request_policies_cors_exposed_headers
          is_allow_credentials_enabled = var.deployment_specification_routes_request_policies_cors_is_allow_credentials_enabled
          max_age_in_seconds           = var.deployment_specification_routes_request_policies_cors_max_age_in_seconds
        }
      }
    }
  }

  #Optional
  display_name = var.deployment_display_name
}