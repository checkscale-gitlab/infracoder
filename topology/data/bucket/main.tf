resource "oci_objectstorage_bucket" "service_bucket" {
  compartment_id = var.compartment_ocid
  namespace      = data.oci_objectstorage_namespace.ns.namespace
  access_type    = var.access_type
  name           = var.display_name
}

resource "oci_objectstorage_namespace_metadata" "namespace-metadata" {
  namespace                    = data.oci_objectstorage_namespace.ns.namespace
  default_s3compartment_id     = var.compartment_ocid
  default_swift_compartment_id = var.compartment_ocid
}