## Getting Started on a Fastrack

The purpose of the fasttrack is, to describe the end to end process in a concise way and enable engineers without experience to prepare a tenant for automated deployments. We provide a path to setup a tenant from configuring a single service, creating artifacts to deploying a the service as part of a topology in five steps. This path helps to navigate the content, every step refers to a separate module that explains the code and the behind the task. At the end you will have a development environment running in OCI that enables to develop infrastructure code remotely.  

### Prerequisites

In order to get started we need access to an OCI tenant, the easiest way to obtain a tenant is **signing up for [Oracle's Free Tier](https://www.oracle.com/cloud/free/)**. And you should have [GitLab account](https://www.gitlab.com) to share code, enable version control and build a repository for your own code. The following tools are required on your local machine:

* [VirtualBox](https://www.virtualbox.org/)
* [VS Code](https://code.visualstudio.com/)
* [Git client](https://git-scm.com)

A modular editor like [VS Code](https://code.visualstudio.com/download), can be extended by essential plugins like the [Terraform](https://marketplace.visualstudio.com/items?itemName=mauve.terraform) to customize the linting and auto-completion function for HCL and bash code. The desktop requires sufficient internet access, in company networks make sure that the VPN is open for protocols like SSH (Port 22) and SQL Net (Port 1521). Oracle employees need to connect via **clear internet**.

### Download the InfraCoder Image

Our example code is written for the Linux CLI and created the [InfraCoder Image](http://bit.ly/2EPHgyb) to have a common base. The image installs [Oracle Linux 7.7](https://www.oracle.com/technetwork/server-storage/linux/downloads/index.html) on a local server is used to develop configuration files and manage the OCI. Getting more used to manage infrastructure via configuration files and the OCI environment you might consider moving to the [cloudshell](https://docs.cloud.oracle.com/en-us/iaas/Content/API/Concepts/cloudshellintro.htm) and the [ressource manager](https://docs.cloud.oracle.com/en-us/iaas/Content/ResourceManager/Concepts/resourcemanager.htm) instead.

Linux is the operating system for most cloud server, using the same operating system locally provides the same set of commands helps to avoid errors originating from different character sets. The default configuration, contains the following tools:

*  [Terraform](https://learn.hashicorp.com/terraform/getting-started/install.html) for topology deployments
*  [Packer](https://www.packer.io/intro/getting-started/install.html) to create custom artifacts
*  [OCI-CLI](https://docs.cloud.oracle.com/iaas/Content/API/SDKDocs/cliinstall.htm) to accelerate template development
*  [Git](https://git-scm.com) for version control and code sharing
*  [jq](https://stedolan.github.io/jq/) as JSON parser

We use [VirtualBox](https://www.virtualbox.org/wiki/Downloads) to launch a local virtual machine (VM). Beside the tools the image contains a couple of scripts that make easier to handle the configuration files. After downloading you can import the image by just double clicking on the OVA file, after that we start the VM in the terminal window.

```
VBoxManage startvm "InfraCoder"
```

The image will setup and use a "host-only" network. The desktop itself acts as a router, what in corporate networks often is prohibited. Please make sure that the desktop runs on an open network when the image is started. In corporate networks these are usually the guest networks. On Windows you might receive an error message. In that case, open the network settings accept the proposed changes and restart the machine. The image starts headless and can be reached via ssh on the local IP *192.168.56.11*. The initial admin user is called *training* and the password *changeme*.

```
ssh training@192.168.56.11
```
NOTE: The image provides an operating system with preinstalled open source software. Usage is at own risk. Oracle is neither responsible nor liable for any damage. All terms and conditions for software packages included with this image remain applicable. 

### Install a Local WebShell

Logging in for the first time, we need to complete the installation of the server by executing the predefined *setup* command. The command requires two arguments a valid email address and a configuration file, "webshell_ol77.sh". The script will be loaded from [GitLab](https://gitlab.com/tboettjer/infracoder). It updates the operating system, installs a couple of tools and creates a default admin before disabling the 'training' user.

```
sudo infracoder <your@email.com> webshell_ol77.sh
```

Select a new username and create a password.
* Preferred username: 
* Create a password: 

The script takes a few minutes to complete. We can follow the installation process, starting a second terminal and reviewing the steps with `tail -f /var/log/infracoder.log`. After successfull installation, an ssh key should show up. When the development server is correctly installed you can logout with `exit`.

```
installation completed, please save the following SSH key, open the browser and login with the new user

-----BEGIN OPENSSH PRIVATE KEY-----
...
-----END OPENSSH PRIVATE KEY-----

```

The terminal shows a [private ssh key](https://www.ssh.com/ssh/key). It is used to establish a secure connection between client and server. Copy the key and store it on your local machine. On Linux and MacOS the default directory is `~/.shh/keyname` on windows it depends on the terminal program, a common choice is [putty](https://support.rackspace.com/how-to/log-into-a-linux-server-with-an-ssh-private-key-on-windows/). 

After setting up the local server we configure the Command Line Interface (CLI) to connect to our tenant. The user can login using a browser. Firefox is recommended for starters who are not familar with adding self-assigned certificates manually.

> https://192.168.56.11/cockpit+app/@localhost/system/terminal.html
 
When everything is installed correctly, the following page should show up.

[<img src="docs/cockpit.png" width="750">](https://192.168.56.11/cockpit+app/@localhost/system/terminal.html)

Note: No official CA will validate the self-signed certificat on the local InfraCoder instance. Requesting the ssh terminal for the first time the browser will show a security violation and request a confirmation, Chrome users need to perform a [manual import](https://www.accuweaver.com/2014/09/19/make-chrome-accept-a-self-signed-certificate-on-osx/) before confirming the request.

> Learn how to write **[Configuration Scripts in Bash](bash.md)**

### Enable Remote Management

Logging into the webshell for the first time we need to setup the command line interface. It adds the oci command to the shell environment and allows to manage OCI ressources remotely. First we check whether all dependencies have been installed.

```
python3 --version && oci -v
```
The response should be a version number, like '3.6.8' for python3 and '2.9.6' for oci. We setup the CLI using the oci command.

```
oci setup config
```

The following information is required and needs to be collected for the [web console](https://console.eu-frankfurt-1.oraclecloud.com/):
* User OCID: 
* Tenancy OCID:
* Home region:

The home region (e.g. "eu-frankfurt-1") is used for identification purposes only. It is not related to the location of the ressources that we deploy. We accept the default options, leave the passphrase empty and answer with 'Y' to create an API key. The configuration script finishes with the creation of the RSA key pair. This is an the API key, not a SSH key. Copy the key and paste it in the web console.

```
cat $HOME/.oci/oci_api_key_public.pem
```

Open the OCI web console,  click your username in the top-right corner of the Console, and then click 'User Settings' and then 'Add Public Key' to paste the contents of the PEM public key in the dialog box. Click 'Add' to complete the setup. The [documentation](https://docs.cloud.oracle.com/iaas/Content/API/Concepts/apisigningkey.htm#How2) describes the upload process in more detail.

<img src="docs/paste_key.png" width="550">

Check whether the new fingerprint matches the one that is stored in `~/.oci/config`. If the fingerprint matches, we select a compartment for our project. with the following command we create a list of compartments, available in the tenant. We use the oci command with the *table* and *query* option to simplify the output in a table.

```
oci iam compartment list --output table --query "data[*].{OCID: id, Name: name}"
```

In case you work in a fresh tenant you need to create a new compartment. Compartment identifiers are globally unique and the number of compartments is limited. It is good practice to avoid deleting and rename unused compartments. Only create a new compartment if updating an unused compartment is not an option. The following command creates a compartment and provides the OCID.

```
~/infracoder/artifacts/bash/create_compartment.sh
```

Copy the OCID of the compartment that you want to use and start the bootstrap process.

```
~/infracoder/artifacts/bash/bootstrap.sh
```

The bootstrap process creates a virtual cloud network (VCN) including a subnet with internet access. A unique project name helps to separate multiple tenants in one OCI account. You can use characters and numbers, refrain from using special characters and empty spaces.

> Get introduced to **[Remote Management using the Command Line Interface (CLI)](cli.md)**

### Introducing State-Awareness

The CLI allows operators to write deployment plans that represent a sequence of configuration steps. Conditionals and loops allow for intelligent execution, but require complex routines. Introducing state-awareness is a significant efficiency leaver. With a tool like Terraform, operatures can track the resources that are deployed and match the current- against the desired architecture. Deployment plans enable incremental adjustments and rapid responses to changing requirements. Operators can proactively support solution oriented teams rather than verify technology oriented requests. They adopt an iterative desgin process for deployment plans along the phases of a SCRUM project. At OCI, Terraform templates are empoyed to automate the launch of single- or multi-server solutions accross data center and regions. 

We translate the existing topology into a topology template using the discovery mechanism of the [OCI service provider](https://www.terraform.io/docs/providers/oci/guides/resource_discovery.html). Discovering existing resources and translating the results into a terraform templates is a unique capability. It helps to introduce state-awareness for an existing topology. The following script collects the tenancy details and captures the current state of the defined compartment. 

```
~/infracoder/artifacts/bash/build_topology.sh
```

With 'terraform init' we generate an initial state file. It is stored as `terraform.tfstate` in the working directory.

```
cd ~/infracoder/topology/<myproject> && terraform init
```

We validate the HCL code using the 'validate' option. In our case, it is more about testing the topology_function() code. However, any errors should be corrected in the templates directly.

```
terraform validate
```

We iterate the code until it doesn't throw any errors anymore, before we move towards deployment planning. The plan matches the current architecture agianst the desired architecture and reports any deviations. 

```
terraform plan
```

Running the command for the first time the current architecture doesn't hold any resources, it will propose to create the vcn and subnet from scratch. Existing resources, created through the cli, are not captured, as these are not captured in the state file. After checking the list deployments we execute the provisioning process. Terraform will create a new vcn, a subnet and the necessary network services.  

```
terraform apply
```

Note: After executing the deployment plan we have two vcn in our compartment. Both vcn are configured using the same CIDR range. OCI network virtualization allows independent IP ranges per vcn, traffic forwarding inside the OCI uses encapsulating, hence the CIDR ranges in seperate vcn can overlap. Before proceeding with code sharing, we delete the vcn that was created, using the oci cli.

```
~/infracoder/artifacts/bash/clean_project.sh
```

> Developing **[Deployment Plans for the Terraform Service Provider](terraform.md)**

### Launching the WebShell as Service on OCI

Topology templates configure and merge artifacts into solution templates. Artifacts are precompiled and prepackaged components that combine operating system and application code to expose a service. We distinguish three different types of artifacts.

* Hosts, multi purpose machines that support configuration scripts for the automation of application deployments
* Node based services, represented by immutable server and build using custom images that are persisted in the image store 
* Container Cluster that are managed through API with customer container retrieved through a private or public registry 

We create artifacts using Packer.  Packer allows enigneers to write configuration files for all three types of artifacts. The build process supports various deployment methods, Packer templates can either be provisioned on bare-metal-, on virtual server or stored as container images. 

```
packer --version
```

We require version 1.5.4. or higher. When the correct packer version is installed, we invoke the packer command inside a shell script to create a webshell artifact in OCI. The following script creates a directory for the HCL templates and creates a custom image in the OCI images store. In our example we use 2.1 shapes, using `VM.Standard.E2.1.Micro` in a Freetier avoids cost. The image source is stored in '~/infracoder/artifacts/bash/create_artifact.sh'.

```
~/infracoder/artifacts/bash/create_artifact.sh webshell_ol77.sh
```

The script builds artifacts from configuration files in the infracoder repository. These are stored in the 'bash' directory. It is important to apply the naming convention that lead to the file name webshell_ol77.sh.

* the service `name` is written in one word and followed by an underscore '_'
* the `_ol77` abbreviation refers to the operating system and the version
* the file exetension `.sh` marks the file as a script. 


The command created a new directory '~/infracoder/topology/myproject'. It contains a deployment plan for the resources that terraform discovered in our compartment. Before we excute the plan, we add an instance of the webshell image. 

```
~/infracoder/artifacts/bash/create_instance.sh webshell
```

State-awareness means, running 'terraform apply' only adds an instance of the WebShell and 'terraform destroy' tiers down the entire stack.

```
cd ~/infracoder/topology/<myproject> && terraform apply
```

The apply command finishes with an output that returns the IP address for the new instance. The following URL opens the webshell on the internet.

> https://<your_IP>/cockpit+app/@localhost/system/terminal.html

When logging in for the first time we need to finish the installation.

```
sudo ~/infracoder/artifacts/bash/create_secrets.sh
```

The script works through a list of commands, copy and paste these commands in the local webshell, to return the values that are required for comleting the setup. After every input hit enter twice to get to the next item on the list.

> Further details on **[Creating Artifacts with Packer](packer.md)**

### Sharing Code through GitLab Repositories

Sharing code in a git repository improves the integrity of virtual resources and reduces administration efforts. Sharing definition files thorugh git allows operators to improve code incrementally and rely on best practices when provsioning the same resources in a different context. A central GitLab repository can either be setup using a predefined a [blueprint](https://about.gitlab.com/install/#centos-7) or subscribing to a [SaaS service](https://www.gitlab.com/). We start with the later.

[<img src="docs/register.png" width="750">](https://gitlab.com/users/sign_in?redirect_to_referer=no)

A Git client is already installed on the InfraCoder image. We check whether git is installed and whether the credentials have been stored correctly. The following command should return a version number and '--list' the current configuration. 

```
git --version && git config --list
```

If everything is correct, we connect our GitLab acoount with the local git client. `cat ~/.ssh/id_rsa.pub` shows the public key generated on the local machine. Mark and copy the key. It is added to the GitLab account through the GitLab web console.

* After login, click your avatar in the upper right corner and select *Settings*.
* Navigating to SSH Keys and pasting the public key in the Key field. 
* Click the Add key button.

> Familerize yourself with **[Manage Deployment Plans on GitLab](git.md)**

### Working with Modules

Topology templates compromise several layers of resources and services. Common guidelines and naming conventions are required to maintain consistency and allow reuse of infrastructure code in a team. Modules allow to organize infrastructure code and separate administrative tasks.

> Gain efficency with **[Managing Terraform Modules](modules.md)**



[<<](README.md) | [+](setup.md) | [>>](bash.md)